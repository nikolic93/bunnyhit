﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class create_bullet : MonoBehaviour {
    public GameObject bullet;
    public GameObject cam;
	
    public void Fire()
    {
        Destroy(Instantiate(bullet, cam.transform.position, cam.transform.rotation), 5f);
    }
}
