﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class GameController : MonoBehaviour {

    public GameObject bullet;
    public TextMesh txtPoints;
    public TextMesh txtTimer;
    public GameObject endSesionPanel;
    public Button btnStartGame;
    public Button btnEndGame;
    public Button btnPlayAgain;
    public Button btnFire;
    public Text txtGameStatus;
    public List<GameObject> targets;

    private int overallPoints;
    private float time;
    private float timeForActiveTarget;
    private int gameLength;
    private bool startTimer;
    private GameObject correctTarget;

	void Start () {
        gameLength = 60;
        startTimer = false;

        btnStartGame.onClick.AddListener(StartGameOnClick);
        btnPlayAgain.onClick.AddListener(StartGameOnClick);
        btnEndGame.onClick.AddListener(EndGameOnClick);

        endSesionPanel.SetActive(false);
        btnStartGame.gameObject.SetActive(true);
        btnPlayAgain.gameObject.SetActive(false);
        btnEndGame.gameObject.SetActive(true);
        btnFire.gameObject.SetActive(false);
	}
	
	void Update () {
        if (startTimer)
        {
            time += Time.deltaTime;
            int timeRemaining = (gameLength - (int)time);
            txtTimer.text = timeRemaining.ToString();
            if (timeRemaining == 0 || overallPoints < 0)
                EndSession("GAME OVER");

            timeForActiveTarget += Time.deltaTime;
            if (timeForActiveTarget > 5)
            {
                SetActiveTarget();
            }
        }
	}

    public void StartGameOnClick()
    {
        endSesionPanel.SetActive(false);
        btnStartGame.gameObject.SetActive(false);
        btnPlayAgain.gameObject.SetActive(false);
        btnEndGame.gameObject.SetActive(false);
        btnFire.gameObject.SetActive(true);
        startTimer = true;

        time = 0;
        overallPoints = 0;
        txtPoints.text = "0";
        SetActiveTarget();
    }

    public void EndGameOnClick()
    {
        Application.Quit();
    }

    public void CheckHit(bool isCorrectHit)
    {
        overallPoints += isCorrectHit ? 1 : -1;
        txtPoints.text = overallPoints.ToString();

        if (overallPoints == 10)
            EndSession("YOU WIN");

        SetActiveTarget();
    }

    private void SetActiveTarget()
    {
        if (correctTarget != null)
            correctTarget.GetComponentInChildren<Anim>().SetInactive();

        var lastCorrectTarget = correctTarget;
        while (lastCorrectTarget == correctTarget)
            correctTarget = targets[Random.Range(0, targets.Count)];

        correctTarget.GetComponentInChildren<Anim>().SetActive();
        timeForActiveTarget = 0;
    }

    void EndSession(string text)
    {
        endSesionPanel.SetActive(true);
        btnStartGame.gameObject.SetActive(false);
        btnPlayAgain.gameObject.SetActive(true);
        btnEndGame.gameObject.SetActive(true);
        btnFire.gameObject.SetActive(false);

        txtGameStatus.text = text;
        startTimer = false;
    }
}
