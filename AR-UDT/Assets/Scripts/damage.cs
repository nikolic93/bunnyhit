﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class damage : MonoBehaviour {

    public GameObject bullet;
    public GameController GameCon;

    void OnTriggerEnter(Collider bunnyCollider)
    {
        GameObject bunny = bunnyCollider.gameObject;
        Anim animationScript = bunny.GetComponent<Anim>();
        Destroy(bullet);
        GameCon.CheckHit(animationScript.isActive);
    }
}
