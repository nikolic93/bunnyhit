﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Anim : MonoBehaviour {

    public Animator anim;
    public bool isActive { get; private set; }

    public void SetActive()
    {
        anim.Play("targetAnimation");
        isActive = true;
    }

    public void SetInactive()
    {
        anim.Play("IdleAnimation");
        isActive = false;
    }
}
